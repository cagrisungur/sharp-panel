import {Component, OnDestroy, OnInit} from '@angular/core';
import { CountryService } from '../../shared/country.service'
import { Params, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-dashborad',
  templateUrl: './dashborad.component.html',
  styleUrls: ['./dashborad.component.scss']
})
export class DashboradComponent implements OnInit, OnDestroy {
  country: any;
  constructor(
    private countryService : CountryService,
    private route: ActivatedRoute,
  ) { }

  getCountry():void{
    this.countryService.getCountry()
    .subscribe(
      resultArray => {this.country = resultArray;
      console.log(this.country);},
      error => console.log('Error::' + error)
    )
  }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.getCountry();
      }
    )
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
  }

    ngOnDestroy(): void {
        document.body.className = '';
    }

}
