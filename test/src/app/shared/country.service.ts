import { Injectable } from '@angular/core';
import { Http, Response, ResponseType } from "@angular/http";
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { importType } from '@angular/compiler/src/output/output_ast';
import { appsettings } from './app.setting';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class CountryService {

    constructor(private http: HttpClient, private route: ActivatedRoute) {
        const routeParams = this.route.snapshot.params;
    }

    getCountry(){
        return this.http.get(appsettings.rootUrl+ '/admin' + '/country', {responseType: 'json'}).map((country)=>{
            console.log(appsettings.rootUrl+ '/admin' + '/country');
            return country;});
    }
    // getCountry() {
    //     return this.http
    //     .get('http://192.168.5.124:9999/admin/country', {
    //         responseType: 'json'
    //     })
    //     .subscribe(
    //     (country) => {
    //         return country;
    //     });
    // }

    
    // getCountry(){
    //     return this.http.get('http://192.168.5.124:9999/admin/country')
    //         .map((response: Response) => response.json());
    // }

}
