import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboradComponent } from './dashboard/dashborad/dashborad.component';
import { LoginComponent } from './login/login.component';
import { CountriesComponent } from "../app/countries/countries.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboradComponent},
  { path: 'countries', component: CountriesComponent},
   {path: '', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
