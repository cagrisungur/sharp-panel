import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CookieService } from 'ngx-cookie-service';
import { config } from '../app/config';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './_services/authentication.service';
import { ReactiveFormsModule }    from '@angular/forms';
import { JwtHelperService, JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { AuthGuardService } from './_services/authGuard.service';
import {DashboardModule} from './dashboard/dashboard.module';
import { Interceptor } from './_services/Interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { PingComponent } from './ping/ping.component';
import { RouterModule } from '@angular/router';
import { CountriesComponent } from './countries/countries.component';
import { CountryService } from './shared/country.service';

const JWT_Module_Options: JwtModuleOptions = {
  config: {
      tokenGetter: () => {
          return localStorage.getItem('ACCESS_TOKEN');
        }
  }
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PingComponent,
    CountriesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule,
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    JwtModule.forRoot(JWT_Module_Options)
  ],
  providers: [CookieService,CountryService, AuthenticationService, AuthGuardService, {
    provide: HTTP_INTERCEPTORS,
    useClass: Interceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
