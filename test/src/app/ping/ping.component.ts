import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-ping',
  templateUrl: './ping.component.html',
  styleUrls: ['./ping.component.scss']
})
export class PingComponent implements OnInit {

  constructor(public http: HttpClient) { }
  public ping() {
    this.http.get('http://192.168.5.124:9999/authorize')
      .subscribe(
        data => console.log(data),
        err => console.log(err)
      );
  }

  ngOnInit() {
  }

}
