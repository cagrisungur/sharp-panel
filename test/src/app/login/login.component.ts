import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../_services/authentication.service';
import { config } from '../config';
import { CookieService } from 'ngx-cookie-service';
import {Md5} from 'ts-md5/dist/md5';
import {Cookie} from 'ng2-cookies/ng2-cookies';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
            
})

export class LoginComponent implements OnInit {
    cookieValue = 'UNKNOWN';
    loginForm: FormGroup;
    submitted = false;
    returnUrl: string;
    error = '';
    expire: any;
    
    constructor(
        private cookieService: CookieService,
        private formBuilder: FormBuilder, 
        public router: Router,
        private authenticationService: AuthenticationService
    ) {}

    ngOnInit() {
       this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
       
        var expire = new Date();
        var time = Date.now();
        expire.setTime(time);
    } 

    get formControl() { 
        return this.loginForm.controls; 
    }

    onSubmit() {
        this.submitted = true;

        if (this.loginForm.invalid) {
           return;
        }

        let password = String(Md5.hashStr(this.formControl.password.value));
        
        this.authenticationService.authorize(this.formControl.username.value, password, config.clientId, config.redirectUri)
            .subscribe(
                data => {
                    let authCode = data.authCode;
                    
                    this.authenticationService.token(authCode, config.clientId, config.clientSecret , config.redirectUri)
                        .subscribe(
                            data => {
                                data.accessToken;
                                data.refreshToken;

                                Cookie.set('ACCESS_TOKEN', data.accessToken);
                                Cookie.set('REFRESH_TOKEN', data.refreshToken);
                                
                                if(data.accessToken != null){
                                    this.router.navigate(['/dashboard']);
                                }

                            },
                            error => {
                                this.error = error;
                            }
                        )
                },
                error => {
                    this.error = error;
                    
                });
    }
}
