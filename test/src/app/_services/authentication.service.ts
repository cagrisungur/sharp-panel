import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CookieService } from 'ngx-cookie-service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable()

export class AuthenticationService {
    constructor(private http: HttpClient, public jwtHelper: JwtHelperService) { }

    authorize(username: string, password: string ,clientId: string, redirectUri: string) {
        let data = new FormData();
        data.append('username', username);
        data.append('password', password);
        data.append('clientId', clientId);
        data.append('redirectUri', redirectUri);

        return this.http.post<any>(`http://192.168.5.124:9999/authorize`, data);
       
    }

    token(authCode: string, clientId: string, clientSecret: string, redirectUri: string) {
        let data = new FormData();
        data.append('code', authCode);
        data.append('clientSecret', clientSecret);
        data.append('clientId', clientId);
        data.append('redirectUri', redirectUri);

        return this.http.post<any>(`http://192.168.5.124:9999/token`, data);
    }

    logout() {
         localStorage.removeItem('currentUser');
    }

    isAuthenticated() : boolean {
        const token = localStorage.getItem('ACCESS_TOKEN');
        
        return !this.jwtHelper.isTokenExpired(token);
    }
    
}