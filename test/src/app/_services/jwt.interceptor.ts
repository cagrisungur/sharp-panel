import 'rxjs/add/operator/do';
import { Observable } from 'rxjs';
import { HttpRequest, HttpResponse, HttpInterceptor, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http'
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';

export class JwtInterceptor implements HttpInterceptor {
  constructor(public auth: AuthenticationService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
       
        }
      }
    });
  }
}