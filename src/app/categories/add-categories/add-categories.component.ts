import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { LanguageService } from '../../_services/language.service';
import { getAllService } from '../../_services/getAll.service';
import { ViewChild, ElementRef } from '@angular/core';
import { ImageTypeService } from '../../_services/imagetype.service';
import { CategoryService } from '../../_services/category.service';
import { SpecificationService } from '../../_services/specification.service';
import { FileTypeService } from '../../_services/filetype.service';
import { FeaturesService } from '../../_services/features.service';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-add-categories',
  templateUrl: './add-categories.component.html',
  styleUrls: ['./add-categories.component.css']
})
export class AddCategoriesComponent implements OnInit {

  @ViewChild('fileInput')
  fileInput: ElementRef;
  
  @ViewChild('imageInput')
  imageInput: ElementRef;

  addForm: FormGroup;
  countries: any[];
  languages: any[];
  text: any[];
  categories: any[];
  specs: any[];
  imageList: any[] = [];
  fileList: any[] = [];
  imageTypes: any[];
  fileTypes: any[];
  features: any[];
  featureTypes: any[];
  categoryFeature: any[];
  categorySpecs: any[];
  item: any[] = [];
  categoryIconImage: any;
  categoryThumbnailImage: any;
  categoryBannerImage: any;
  productAreaBannerImage: any;
  exampleCategoryBanner: any;
  exampleProductBanner: any;
  examplethumbnail: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private getAllService: getAllService,
    private imageTypeService: ImageTypeService,
    private categoryService: CategoryService,
    private specService: SpecificationService,
    private fileTypeService: FileTypeService,
    private featureService: FeaturesService,
    private lightBox: Lightbox

  ) {
    this.categoryIconImage = "http://34.244.173.118:6060/uploads/images/category-icon-formenu.png";
    this.categoryThumbnailImage = "http://34.244.173.118:6060/uploads/images/thumbnail-category.png";
    this.examplethumbnail = "http://34.244.173.118:6060/uploads/images/example-thumbnail.png";
    this.categoryBannerImage = "http://34.244.173.118:6060/uploads/images/category-area-banner.png";
    this.productAreaBannerImage = "http://34.244.173.118:6060/uploads/images/product-area-banner.png";
    this.exampleCategoryBanner = "http://34.244.173.118:6060/uploads/images/banner-category-example.png";
    this.exampleProductBanner = "http://34.244.173.118:6060/uploads/images/prodduct-area-banner-example.png";

   }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.addForm = this.formBuilder.group({
      parentId: '',
      name: '',
      keywords: '',
      priority: '',
      active: true
    });

    this.getAllService.getCountries()
      .subscribe(data => {
        this.countries = data;
        this.text = this.initArray();
      }
      );

    this.categoryService.getCategoryAll()
      .subscribe(data => {
        this.categories = data;
      }
      );

    this.imageTypeService.getImageTypes()
      .subscribe(data => {
        this.imageTypes = data;
      });

    this.fileTypeService.getFileTypes()
      .subscribe(data => {
        this.fileTypes = data;
      });

      this.specService.getSpecifications()
      .subscribe( data => {
        this.specs = data;
        this.categorySpecs = this.initSpecArray();
      });

    this.featureService.getFeatureTypes()
      .subscribe(data => {
        this.featureTypes = data;
        this.featureService.getFeatures()
          .subscribe(data => {
            this.features = data;
            this.categoryFeature = this.initFeatureArray();
          });
      });

  }

  
  showImage(image: any) {
    let album = [{
      src: image,
      caption: '',
      thumb: image
    }];
    this.lightBox.open(album, 0);
  }

  handleImageInput(images: FileList) {
    for(let i = 0; i < images.length; i++) {
      this.imageList.push(images.item(i));
    }
    this.imageInput.nativeElement.value = '';
  }

  handleFileInput(files: FileList) {
    for(let i = 0; i < files.length; i++) {
      this.fileList.push(files.item(i));
    }
    this.fileInput.nativeElement.value = '';
  }


  async createFileListData() {
    let formArray = [];

    let priority = 1;    
    for(let index in this.fileList) {
      const data = await this.fileToBase64(this.fileList[index]);
      
      formArray.push({
        fileCode: this.fileList[index].dType,
        fileName: this.fileList[index].dName,
        priority: priority++,
        fileBase64: data
      });
    }

    return formArray;
  }

 
  async createImageListData() {
    let formArray = [];

    let priority = 1;
    for(let index in this.imageList) {
      const data = await this.fileToBase64(this.imageList[index]);

      formArray.push({
        imageCode: this.imageList[index].dType,
        name: this.imageList[index].dName,
        active: 1,
        priority: this.imageList[index].priority,
        imageFile: data
      });
    }

    return formArray;
  }

  async fileToBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  initArray() {
    var formArray = [];

    for (let i = 0; i < this.countries.length; i++) {
      formArray[this.countries[i].id] = [];
      for (let j = 0; j < this.countries[i].languages.length; j++) {
        formArray[this.countries[i].id][this.countries[i].languages[j].id] = {
          name: '',
          shortDesc: '',
          longDesc: '',
          keywords: '',
          buttonLink: '',
          buttonName: '',
          subDesc: '',
          subTitle: '',
        };
      }
    }
    return formArray;
  }

  initFeatureArray() {
    var formArray = [];

    for (let i = 0; i < this.features.length; i++) {
      formArray[this.features[i].id] = {
        value: false,
        type: 0
      };
    }

    return formArray;
  }

  initSpecArray() {
    var formArray = [];

    for(let i = 0; i < this.specs.length; i++) {
      formArray[this.specs[i].id] = '';
    }
    
    return formArray;
  }

  async onSubmit() {
    let postData = this.addForm.value;
    postData.text = this.createTextObject();
    postData.image = await this.createImageListData();
    postData.feature = this.createCategoryFeatureData();
    postData.spec = this.createCategorySpecsData();
    postData.file = await this.createFileListData();
    postData.active = postData.active ? 1 : 0;

    this.categoryService.createCategory([postData])
      .subscribe(
        data => {
           this.router.navigate(['category']);
        },
        error => {
          alert(error);
        });
  }

  createTextObject() {
    let textArray = [];
    for (let i = 0; i < this.countries.length; i++) {
      for (let j = 0; j < this.countries[i].languages.length; j++) {
        var nameValue = this.text[this.countries[i].id][this.countries[i].languages[j].id].name;
        var longDesc = this.text[this.countries[i].id][this.countries[i].languages[j].id].longDesc;
        var shortDesc = this.text[this.countries[i].id][this.countries[i].languages[j].id].shortDesc;
        var keyValue = this.text[this.countries[i].id][this.countries[i].languages[j].id].keywords;
        var subTitle = this.text[this.countries[i].id][this.countries[i].languages[j].id].subTitle;
        var subDesc = this.text[this.countries[i].id][this.countries[i].languages[j].id].subDesc;
        var buttonName = this.text[this.countries[i].id][this.countries[i].languages[j].id].buttonName;
        var buttonLink = this.text[this.countries[i].id][this.countries[i].languages[j].id].buttonLink;
        if( nameValue || longDesc || shortDesc || keyValue || subTitle || subDesc || buttonName || buttonLink != ""){
          textArray.push({
            countryId: this.countries[i].id,
            langId: this.countries[i].languages[j].id,
            name: nameValue,
            longDesc: longDesc,
            shortDesc: shortDesc,
            keywords: keyValue,
            buttonLink: buttonLink,
            buttonName: buttonName,
            subDesc: subDesc,
            subTitle: subTitle,
          })
        }

      }
    }
    return textArray;
  }

  createCategorySpecsData() {
    let formArray = [];

    let priority = 1;
    for(let index in this.categorySpecs) {
      if(this.categorySpecs[index] && this.categorySpecs[index] != '') {
        formArray.push({
          'specId': index,
          'value': this.categorySpecs[index],
          'priority': priority++
        });
      }
      
    }

    return formArray;
  }

  createCategoryFeatureData() {
    let formArray = [];

    let priority = 1;
    for (let index in this.categoryFeature) {
      if (this.categoryFeature[index].value) {
        formArray.push({
          'featureId': index,
          'typeId': this.categoryFeature[index].type,
          'priority': priority++
        });
      }
    }

    return formArray;
  }

}


