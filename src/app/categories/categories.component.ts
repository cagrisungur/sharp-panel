import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../_services/category.service';
import { Category } from '../models/category.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: Category[];
  
  constructor(
    private router: Router, 
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.categoryService.getCategoryAll()
    .subscribe( data => {
      this.categories = data;
      setTimeout(() => {
        $('#dataTable').DataTable({
          responsive: true
        });
      }, 100);
    });
  }

  deleteCategory(category:any): void{
    this.categoryService.deleteCategory(category.id)
      .subscribe( data => {
        this.categories = this.categories.filter(u => u !== category);
      })
  }

  editCategory(category: any): void {
    this.router.navigate(['edit-category', category.id]);
  };

  addCategory(): void {
    this.router.navigate(['add-category']);
  };

}
