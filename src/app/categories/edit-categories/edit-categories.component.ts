import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from "@angular/forms";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { getAllService } from '../../_services/getAll.service';
import { ViewChild, ElementRef } from '@angular/core';
import { ImageTypeService } from '../../_services/imagetype.service';
import { CategoryService } from '../../_services/category.service';
import { SpecificationService } from '../../_services/specification.service';
import { FileTypeService } from '../../_services/filetype.service';
import { FeaturesService } from '../../_services/features.service';
import { post } from 'selenium-webdriver/http';
import { Lightbox } from 'ngx-lightbox';


@Component({
  selector: 'app-edit-categories',
  templateUrl: './edit-categories.component.html',
  styleUrls: ['./edit-categories.component.css']
})
export class EditCategoriesComponent implements OnInit {

  @ViewChild('fileInput')
  fileInput: ElementRef;

  @ViewChild('imageInput')
  imageInput: ElementRef;

  editForm: FormGroup;
  countries: any[];
  languages: any[];
  text: FormArray = new FormArray([]);
  textList: any[];
  categories: any[];
  specs: any[];
  imageList: any[] = [];
  fileList: any[] = [];
  imageTypes: any[];
  fileTypes: any[];
  features: any[];
  featureTypes: any[];
  categoryFeature: any[];
  spec: any[];
  categoryId: any;
  categoryIconImage: any;
  categoryThumbnailImage: any;
  categoryBannerImage: any;
  productAreaBannerImage: any;
  exampleCategoryBanner: any;
  exampleProductBanner: any;
  examplethumbnail: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private getAllService: getAllService,
    private imageTypeService: ImageTypeService,
    private categoryService: CategoryService,
    private specService: SpecificationService,
    private fileTypeService: FileTypeService,
    private featureService: FeaturesService,
    private route: ActivatedRoute,
    private lightBox: Lightbox

  ) {
    this.categoryIconImage = "http://34.244.173.118:6060/uploads/images/category-icon-formenu.png";
    this.categoryThumbnailImage = "http://34.244.173.118:6060/uploads/images/thumbnail-category.png";
    this.examplethumbnail = "http://34.244.173.118:6060/uploads/images/example-thumbnail.png";
    this.categoryBannerImage = "http://34.244.173.118:6060/uploads/images/category-area-banner.png";
    this.productAreaBannerImage = "http://34.244.173.118:6060/uploads/images/product-area-banner.png";
    this.exampleCategoryBanner = "http://34.244.173.118:6060/uploads/images/banner-category-example.png";
    this.exampleProductBanner = "http://34.244.173.118:6060/uploads/images/prodduct-area-banner-example.png";

   }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.route.params
      .subscribe(params => {
        this.categoryId = params["categoryid"];
        this.editForm = this.formBuilder.group({
          parentId: 0,
          name: '',
          keywords: '',
          priority: '',
          active: true
        });
        this.categoryService.getCategoryById(this.categoryId)
          .subscribe(data => {
            let obj = data;
            let formData = {};
            formData['parentId'] = data.parentId;
            formData['name'] = data.name;
            formData['keywords'] = data.keywords;
            formData['priority'] = data.priority;
            formData['active'] = data.active == 1 ? true : false;

            this.getAllService.getCountries()
              .subscribe(data => {
                this.countries = data;
                this.textList = this.initArray(obj);
                this.imageList = this.initImageList(obj);
                this.fileList = this.initFileList(obj);

              });


            this.featureService.getFeatureTypes()
              .subscribe(data => {
                this.featureTypes = data;
                this.featureService.getFeatures()
                  .subscribe(data => {
                    this.features = data;
                    this.categoryFeature = this.initFeatureArray(obj);
                  });
              }
              );

            this.specService.getSpecifications()
              .subscribe(data => {
                this.specs = data;
                this.createSpecList(obj);
              });


            this.editForm.setValue(formData);
          });
      });

    this.editForm = this.formBuilder.group({
      parentId: 0,
      name: '',
      keywords: '',
      priority: '',
      active: true
    });

    this.categoryService.getCategoryAll()
      .subscribe(data => {
        this.categories = data;
      }
      );

    this.imageTypeService.getImageTypes()
      .subscribe(data => {
        this.imageTypes = data;
      });

    this.fileTypeService.getFileTypes()
      .subscribe(data => {
        this.fileTypes = data;
      });
  }

  initArray(obj: any) {

    var formArray = [];

    for (let i = 0; i < this.countries.length; i++) {
      formArray[this.countries[i].id] = [];
      for (let j = 0; j < this.countries[i].languages.length; j++) {
        let filters = {
          countryId: this.countries[i].id,
          langId: this.countries[i].languages[j].id
        }

        let foundList = obj.text.filter(
          text => filters.countryId == text.country_id && filters.langId == text.lang_id
        );
        formArray[this.countries[i].id][this.countries[i].languages[j].id] = foundList[0] ? foundList[0] : {
          name: '',
          shortDesc: '',
          longDesc: '',
          keywords: '',
          buttonLink: '',
          buttonName: '',
          subDesc: '',
          subTitle: '',
        };
      }
    }
    return formArray;
  }


  initImageList(obj: any) {
    var formArray = [];
    for (let i = 0; i < obj.image.length; i++) {
      if (obj.image[i].path.endsWith('.jpeg') || obj.image[i].path.endsWith('.png') || obj.image[i].path.endsWith('.jpg')) {
        formArray.push({
          id: obj.image[i].id,
          dName: obj.image[i].name,
          path: obj.image[i].path,
          imageCode: obj.image[i].type.code,
          active: obj.image[i].active,
          priority: obj.image[i].priority,
          convertBase64: false
        });
      }
    }
    return formArray;
  }

  showImage(image: any) {
    let album = [{
      src: image.path,
      caption: '',
      thumb: image.path
    }];
    this.lightBox.open(album, 0);
  }

  showImage2(image: any) {
    let album = [{
      src: image,
      caption: '',
      thumb: image
    }];
    this.lightBox.open(album, 0);
  }

  async fileToBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);

    });
  }
  createSpecObject() {
    let formArray = [];

    for (let i = 0; i < this.specs.length; i++) {
      if (this.specs[i].value && this.specs[i].value != '') {
        formArray.push({
          specId: this.specs[i].id,
          value: this.specs[i].value,
          priority: this.specs[i].priority
        });
      }

    }

    return formArray;
  }


  createSpecList(obj: any) {
    for (let i = 0; i < obj.spec.length; i++) {
      for (let k = 0; k < this.specs.length; k++) {
        if (obj.spec[i].spec_id == this.specs[k].id) {
          this.specs[k].value = obj.spec[i].value;
          this.specs[k].priority = obj.spec[i].priority;
        }
      }
    }
  }

  initFeatureArray(obj) {
    var formArray = [];

    for (let i = 0; i < this.features.length; i++) {
      let filters = {
        featureId: this.features[i].id
      };

      let foundList = obj.feature.filter(
        feature => filters.featureId == feature.feature.id
      );

      formArray[this.features[i].id] = {
        value: foundList[0] ? true : false,
        type: foundList[0] ? foundList[0].type.id : 0,
        old: false
      };
    }

    return formArray;
  }

  createCategoryFeatureData() {
    let formArray = [];

    let priority = 1;
    for (let index in this.categoryFeature) {
      if (this.categoryFeature[index].old != false) {
        if (this.categoryFeature[index].value) {
          if (this.categoryFeature[index].type &&
            this.categoryFeature[index].type.id != '') {
            formArray.push({
              'featureId': index,
              'featureType': this.categoryFeature[index].type,
              'typeId': this.categoryFeature[index].type.id,
              'priority': priority++,
              isOld: true

            });
          }
        }
      }
      else {
        if (this.categoryFeature[index].type &&
          this.categoryFeature[index].type.id != '') {
          formArray.push({
            'featureId': index,
            'featureType': this.categoryFeature[index].type,
            'typeId': this.categoryFeature[index].type.id,
            'priority': priority++,
            isOld: false,
            isDeleted: this.categoryFeature[index].isDeleted
          });
        }
      }
    }

    return formArray;
  }

  handleImageInput(images: FileList) {
    for (let i = 0; i < images.length; i++) {
      this.imageList.push(images.item(i));
    }
    this.imageInput.nativeElement.value = '';
  }

  handleFileInput(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.fileList.push(files.item(i));
    }
    this.fileInput.nativeElement.value = '';
  }


  delete(images, imageIndex){
    if(this.imageList[imageIndex].convertBase64 == false){
      images.isDeleted = true; 

    }
    else{
      this.imageList.splice(imageIndex, 1);

    }
    
  }

  async createImageListData() {

    let formArray = [];
    let priority = 1;
    for (let index in this.imageList) {
      if (this.imageList[index].convertBase64 != false) {
        const data = await this.fileToBase64(this.imageList[index]);
        formArray.push({
          imageCode: this.imageList[index].imageCode,
          name: this.imageList[index].dName,
          active: this.imageList[index].active,
          priority: this.imageList[index].priority,
          imageFile: data,
          isOld: false
        });
      } else {
        formArray.push({
          id: this.imageList[index].id,
          imageCode: this.imageList[index].imageCode,
          name: this.imageList[index].dName,
          active: this.imageList[index].active,
          priority: this.imageList[index].priority,
          imageFile: this.imageList[index].path,
          isOld: true,
          isDeleted: this.imageList[index].isDeleted
        });
      }
    }

    return formArray;
  }

  initFileList(obj: any) {
    let formArray = [];
    for (let i = 0; i < obj.file.length; i++) {
      formArray.push({
        id: obj.file[i].file_id,
        path: obj.file[i].file.path,
        dName: obj.file[i].file.name,
        fileCode: obj.file[i].file.type.code,
        priority: obj.file[i].priority,
        conBase64: false
      });
    }
    return formArray;
  }
  
  deletefile(file, fileIndex){
    if(this.fileList[fileIndex].convertBase64 == false){
      file.isDeleted = true; 

    }
    else{
      this.imageList.splice(fileIndex, 1);

    }
    
  } 

  async createFileListData() {
    let formArray = [];
    let priority = 1;
    for (let index in this.fileList) {
      if (this.fileList[index].conBase64 != false) {
        const data = await this.fileToBase64(this.fileList[index]);

        formArray.push({
          id: this.fileList[index].id,
          fileCode: this.fileList[index].fileCode,
          fileName: this.fileList[index].dName,
          priority: this.fileList[index].priority,
          fileBase64: data,
          isOld: false
        });
      } else {
        formArray.push({
          id: this.fileList[index].id,
          fileCode: this.fileList[index].fileCode,
          fileName: this.fileList[index].dName,
          priority: this.fileList[index].priority,
          fileBase64: this.fileList[index].path,
          isOld: true,
          isDeleted: this.fileList[index].isDeleted
        });
      }
    }

    return formArray;

  }



  createCategorySpecsData() {
    let formArray = [];

    let priority = 1;
    for (let index in this.spec) {
      if (this.spec[index] && this.spec[index] != '') {

        formArray.push({
          'specId': index,
          'value': this.spec[index],
          'priority': priority++
        });
      }

    }

    return formArray;
  }



  async onSubmit() {
    let postData = this.editForm.value;
    postData.text = this.createCountryLangObject();
    postData.image = await this.createImageListData();
    postData.file = await this.createFileListData();
    postData.spec = this.createSpecObject();
    postData.feature = this.createCategoryFeatureData();
    postData.active = postData.active ? 1 : 0;
    this.categoryService.updateCategory(this.categoryId, postData)
      .subscribe(
        data => {
          this.router.navigate(['category']);
        },
        error => {
          alert(error);
        });
  }

  createCountryLangObject() {
    let textArray = [];

    for (let i = 0; i < this.countries.length; i++) {
      for (let j = 0; j < this.countries[i].languages.length; j++) {

        var nameValue = this.textList[this.countries[i].id][this.countries[i].languages[j].id].name;
        var shortDesc = this.textList[this.countries[i].id][this.countries[i].languages[j].id].shortDescription;
        var longDesc = this.textList[this.countries[i].id][this.countries[i].languages[j].id].longDescription;
        var keyValue = this.textList[this.countries[i].id][this.countries[i].languages[j].id].keywords;
        var subTitle = this.textList[this.countries[i].id][this.countries[i].languages[j].id].subtitle;
        var subDesc = this.textList[this.countries[i].id][this.countries[i].languages[j].id].subDescription;
        var buttonName = this.textList[this.countries[i].id][this.countries[i].languages[j].id].button;
        var buttonLink = this.textList[this.countries[i].id][this.countries[i].languages[j].id].buttonLink;

        if (nameValue || shortDesc || longDesc || keyValue || subTitle || subDesc || buttonName || buttonLink != '') {
          textArray.push({
            countryId: this.countries[i].id,
            langId: this.countries[i].languages[j].id,
            name: nameValue,
            shortDesc: shortDesc,
            longDesc: longDesc,
            keywords: keyValue,
            buttonLink: buttonLink,
            buttonName: buttonName,
            subDesc: subDesc,
            subTitle: subTitle,
          })
         }
      }
    }
    return textArray;
  }



}


