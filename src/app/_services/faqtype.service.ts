import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable()
export class FaqTypeService {
 
  url: string = config.baseUrl + '/'  + 'admin/faqtype';

  constructor(private http: HttpClient) { }

  getFaqTypes() {
    return this.http.get<any[]>(this.url);
  }

  getFaqTypeById(id: number) {
    return this.http.get<any>(this.url + '/' + id);
  }

  createFaqType(faqtype: any) {
    return this.http.post(this.url, faqtype);
  }

  updateFaqType(faqtypeId:string, faqtype: any) {
    console.log(this.url);
    return this.http.put(this.url + '/' + faqtypeId, faqtype);
  }

  deleteFaqType(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

}
