import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable()
export class NewsService {

  url: string = config.baseUrl + '/'  + 'admin/news';

  constructor(private http: HttpClient) { }

  getNews() {
    return this.http.get<any[]>(this.url);
  }

  getNewsById(id: number) {
    return this.http.get<any>(this.url + '/' + id);
  }

  createNews(news: any) {
    return this.http.post(this.url, news);
  }

  updateNews(newsId:string, news: any) {
    console.log(this.url);
    return this.http.put(this.url + '/' + newsId, news);
  }

  deleteNews(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

}
