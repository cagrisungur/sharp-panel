import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable()
export class PublishService {

  publish: any;
  url: string = config.baseUrl + '/'  + 'admin/publish';

  constructor(private http: HttpClient) { }

  getPublish() {
    return this.http.get<any>(this.url);
  }

  createPublish(publish: any) {
    return this.http.post(this.url, publish);
  }

}
