import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable()
export class LogoService {

  url: string = config.baseUrl + '/'  + 'admin/logo';

  constructor(private http: HttpClient) { }

  getLogo() {
    return this.http.get<any[]>(this.url);
  }

  getLogoById(id: number) {
    return this.http.get<any>(this.url + '/' + id);
  }

  createLogo(logo: any) {
    return this.http.post(this.url, logo);
  }

  updateLogo(logoId:string, logo: any) {
    return this.http.put(this.url + '/' + logoId, logo);
  }

  deleteLogo(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

}
