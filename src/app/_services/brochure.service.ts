import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable()
export class BrochureService {

  url: string = config.baseUrl + '/'  + 'admin/brochure';

  constructor(private http: HttpClient) { }

  getBrochure() {
    return this.http.get<any[]>(this.url);
  }

  getBrochureById(id: number) {
    return this.http.get<any>(this.url + '/' + id);
  }

  createBrochure(brochure: any) {
    return this.http.post(this.url, brochure);
  }

  updateBrochure(brochureId:string, brochure: any) {
    return this.http.put(this.url + '/' + brochureId, brochure);
  }

  deleteBrochure(id: number) {
    return this.http.delete(this.url + '/' + id);
  }

}
