import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable()
export class ModulesService {
  createProfile(pagetypeArray: any[]): any {
    throw new Error("Method not implemented.");
  }

  module: any[];
  url: string = config.baseUrl + '/'  + 'admin/modules';

  constructor(private http: HttpClient) { }

  getModules() {
    return this.http.get<any[]>(this.url);
  }
 
  getModulesById(id: number) {
    return this.http.get<any>(this.url + '/' + id);
  }

  createModules(module: any) {
    return this.http.post(this.url, module);
  }

  updateModules(moduleId:string, module: any) {
    console.log(this.url);
    return this.http.put(this.url + '/' + moduleId, module);
  }

  deleteModules(id: number) {
    return this.http.delete(this.url + '/' + id);
  }
}
