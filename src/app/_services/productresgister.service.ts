import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../config';

@Injectable()
export class ProductRegisterService {

  productRegister: any[];
  url: string = config.baseUrl + '/' + 'admin/productregister';

  constructor(private http: HttpClient) { }

  getProductRegister() {
    return this.http.get<any[]>(this.url);
  }
  
  getProductRegisterById(id: number) {
    return this.http.get<any>(this.url + '/' + id);
  }
}
