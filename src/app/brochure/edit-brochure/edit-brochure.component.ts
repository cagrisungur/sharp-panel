import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { BrochureService } from '../../_services/brochure.service';
import { getAllService } from '../../_services/getAll.service';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-edit-brochure',
  templateUrl: './edit-brochure.component.html',
  styleUrls: ['./edit-brochure.component.scss']
})
export class EditBrochureComponent implements OnInit {
  @ViewChild('imageInput')
  imageInput: ElementRef;

  @ViewChild('fileInput')
  fileInput: ElementRef;
  editForm: FormGroup;
  brochureArray: any[];
  languages: FormArray = new FormArray([]);
  private brochureId: string;
  countries: any[];
  brochureFile: any;
  fileList: any[] = [];
  imageList: any[] = [];
  brochureImages: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private brochureService: BrochureService,
    private getAllService: getAllService,
    private lightBox: Lightbox

  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.route.params
      .subscribe(
        (params: Params) => {
          this.brochureId = params['brochureid'];
          this.editForm = this.formBuilder.group({
            name: '',
            countryId: '',
            text: '',
            link: '',
            thumbnail: '',
            active: true,
            priority: 0,
            
          });
          this.brochureService.getBrochureById(+this.brochureId)
            .subscribe(data => {
              let formData = [];
              this.brochureFile = data.link;
              this.brochureImages = data.thumbnail;
              formData['name'] = data.name;
              formData['countryId'] = data.country.id;
              formData['text'] = data.text;
              formData['active'] = data.active;
              formData['priority'] = data.priority;
              formData['link'] = data.link;
              formData['thumbnail'] = data.thumbnail;
              this.editForm.setValue(formData);
              this.editForm.value.isLinkOld = true;
              this.editForm.value.isThumbnailOld = true;

            });
          this.getAllService.getCountries()
            .subscribe(data => {
              this.countries = data;
            }
            );
        }
      )

    this.brochureService.getBrochure()
      .subscribe(data => {
        this.brochureArray = data;
      });
  }

  showImage(image: any) {
    let album = [{
      src: image,
      caption: '',
      thumb: image
    }];
    this.lightBox.open(album, 0);
  }

  async handleImageInput(images: FileList) {
    for (let i = 0; i < images.length; i++) {
      this.imageList.push(images.item(i));
    }
    const data = await this.fileToBase64(this.imageList[0]);
    this.editForm.value.thumbnail = data;
    this.editForm.value.isLinkOld = false;
    this.imageInput.nativeElement.value = '';
  }

  async handleFileInput(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.fileList.push(files.item(i));
    }
    const fileData = await this.fileToBase64(this.fileList[0]);
    this.editForm.value.link = fileData;
    this.editForm.value.isThumbnailOld = false;
    this.fileInput.nativeElement.value = '';
  }

  async fileToBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  async onSubmit() {
    this.brochureService.updateBrochure(this.brochureId, this.editForm.value)
      .subscribe(
        data => {
          this.router.navigate(['brochure']);
        },
        error => {
          alert(error);
        });
  }
}

