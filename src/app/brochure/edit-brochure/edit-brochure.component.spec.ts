import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditBrochureComponent } from './edit-brochure.component';

describe('EditBrochureComponent', () => {
  let component: EditBrochureComponent;
  let fixture: ComponentFixture<EditBrochureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditBrochureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditBrochureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
