import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { Router } from "@angular/router";
import { BrochureService } from '../../_services/brochure.service';
import { getAllService } from '../../_services/getAll.service';

@Component({
  selector: 'app-add-brochure',
  templateUrl: './add-brochure.component.html',
  styleUrls: ['./add-brochure.component.scss']
})
export class AddBrochureComponent implements OnInit {
  @ViewChild('imageInput')
  imageInput: ElementRef;

  @ViewChild('fileInput')
  fileInput: ElementRef;
  
  addForm: FormGroup;
  imageList: any[] = [];
  public thumbnail: String = '';
  countries: any[];
  fileList: any[] = [];
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private getAllService: getAllService,
    private brochureService: BrochureService
  ) { }

  async ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.addForm = this.formBuilder.group({
      name: '',
      countryId: '',
      text: '',
      link: '',
      thumbnail: '',
      active: true,
      priority: '',
    });

    this.getAllService.getCountries()
      .subscribe(data => {
        this.countries = data;
      }
      );
  }

  handleImageInput(images: FileList) {
    for (let i = 0; i < images.length; i++) {
      this.imageList.push(images.item(0));
    }
  }

  handleFileInput(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.fileList.push(files.item(0));
    }
  }

  async fileToBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  async onSubmit() {
    let brochureArray = [];
    const data = await this.fileToBase64(this.imageList[0]);
    const fileData = await this.fileToBase64(this.fileList[0]);
    this.addForm.value.thumbnail = data;
    this.addForm.value.link = fileData;
    brochureArray.push(this.addForm.value);
    this.brochureService.createBrochure(brochureArray)
      .subscribe(data => {
        this.router.navigate(['brochure']);
      });
  }

}
