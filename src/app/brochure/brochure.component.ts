import { Component, OnInit } from '@angular/core';
import { Language } from '../models/language.model';
import { Router } from '@angular/router';
import { BrochureService } from '../_services/brochure.service';

@Component({
  selector: 'app-brochure',
  templateUrl: './brochure.component.html',
  styleUrls: ['./brochure.component.scss']
})
export class BrochureComponent implements OnInit {

  brochure: any[]; 
  
  constructor(
    private router: Router, 
    private brochureService: BrochureService
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.brochureService.getBrochure()
      .subscribe(data => {
        this.brochure = data;
        setTimeout(() => {
          $('#dataTable').DataTable({
            responsive: true
          });
        }, 100);
      });
  }

  deleteBrochure(brochure: any): void {
    this.brochureService.deleteBrochure(brochure.id)
      .subscribe(data => {
        this.brochure = this.brochure.filter(u => u !== brochure);
        console.log(this.brochure);
      })
  };

  editBrochure(brochure: any): void {
    this.router.navigate(['edit-brochure', brochure.id]);
  };

  addBrochure(): void {
    this.router.navigate(['add-brochure']);
  };
}
