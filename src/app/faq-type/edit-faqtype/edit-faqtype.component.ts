import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, FormGroup} from "@angular/forms";
import { FaqTypeService } from '../../_services/faqtype.service';

@Component({
  selector: 'app-edit-faqtype',
  templateUrl: './edit-faqtype.component.html',
  styleUrls: ['./edit-faqtype.component.css']
})
export class EditFaqTypeComponent implements OnInit {

  faqtype: any;
  editForm: FormGroup;
  private faqtypeId: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private faqTypeService : FaqTypeService,
    private route: ActivatedRoute)
   { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.route.params
      .subscribe(
        (params: Params) => {
          this.faqtypeId = params['faqtypeid'];
          this.editForm = this.formBuilder.group({
            code: '',
            name: '',
          });
          this.faqTypeService.getFaqTypeById(+this.faqtypeId)
            .subscribe(data => {
              let formData = [];
              formData['name'] = data.name;
              formData['code'] = data.code;
              this.editForm.setValue(formData);
            });
        }
      )
  }

  onSubmit() {
    this.faqTypeService.updateFaqType(this.faqtypeId, this.editForm.value)
      .subscribe(
        data => {
          this.router.navigate(['faqtype']);
        },
        error => {
          alert(error);
        });
  }

}

