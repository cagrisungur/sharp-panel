import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, FormGroup} from "@angular/forms";
import { FaqTypeService } from '../../_services/faqtype.service';

@Component({
  selector: 'app-faqtype',
  templateUrl: './add-faqtype.component.html',
  styleUrls: ['./add-faqtype.component.css']
})
export class AddFaqTypeComponent implements OnInit {

  addForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private  faqtypeService : FaqTypeService ) 
  { }

    
  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.addForm = this.formBuilder.group({
      name: '',
      code: ''
    });
  }

  onSubmit() {
    let postData = this.addForm.value;
    postData.active = postData.active ? 1 : 0;

  this.faqtypeService.createFaqType([postData])
  .subscribe(
    data => {
      this.router.navigate(['faqtype']);
    },
    error => {
      alert(error);
    });
}

}
