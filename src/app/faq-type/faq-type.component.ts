import { Component, OnInit } from '@angular/core';
import { FaqTypeService } from '../_services/faqtype.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-faq-type',
  templateUrl: './faq-type.component.html',
  styleUrls: ['./faq-type.component.scss']
})
export class FaqTypeComponent implements OnInit {

  faqtype: any;

  constructor(
    private router: Router, 
    private faqTypeService: FaqTypeService
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
  
    this.faqTypeService.getFaqTypes()
      .subscribe(data => {
        this.faqtype = data;
        setTimeout(() => {
          $('#dataTable').DataTable({
            responsive: true
          });
        }, 100);
      });
  }

  deleteFaqType(faqtype: any): void {
    this.faqTypeService.deleteFaqType(faqtype.id)
      .subscribe(data => {
        this.faqtype = this.faqtype.filter(u => u !== faqtype);
      })
  };

  editFaqType(faqtype: any): void {
    this.router.navigate(['edit-faqtype', faqtype.id]);
  };

  addFaqType(): void {
    this.router.navigate(['add-faqtype']);
  };
}