import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { Router } from "@angular/router";
import { LogoService } from '../../_services/logo.service';


@Component({
  selector: 'app-add-logo',
  templateUrl: './add-logo.component.html',
  styleUrls: ['./add-logo.component.scss']
})
export class AddLogoComponent implements OnInit {
  @ViewChild('imageInput')
  imageInput: ElementRef;
  @ViewChild('fileInput')
  fileInput: ElementRef;
  data: any;
  addForm: FormGroup;
  imageList: any[] = [];
  fileList: any[] = [];

  public thumbnail: String = '';
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private logoService: LogoService
  ) { }

  handleImageInput(images: FileList) {
    for (let i = 0; i < images.length; i++) {
      this.imageList.push(images.item(0));
    }
  }

  handleFileInput(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.fileList.push(files.item(0));
    }
  }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.addForm = this.formBuilder.group({
      name: '',
      thumbnail: '',
      link: '',
      active: true,
      priority: '',
    });
  }



  async fileToBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  async onSubmit() {
    let logoArray = [];
    const data = await this.fileToBase64(this.imageList[0]);
    const fileData = await this.fileToBase64(this.fileList[0]);
    this.addForm.value.thumbnail = data;
    this.addForm.value.link = fileData;
    logoArray.push(this.addForm.value);
    this.logoService.createLogo(logoArray)
      .subscribe(data => {
        this.router.navigate(['logo']);
      });
  }

}
