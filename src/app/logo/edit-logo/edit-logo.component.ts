import { Component, OnInit, ElementRef,ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { LogoService } from '../../_services/logo.service';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-edit-logo',
  templateUrl: './edit-logo.component.html',
  styleUrls: ['./edit-logo.component.scss']
})
export class EditLogoComponent implements OnInit {
  @ViewChild('fileInput')
  fileInput: ElementRef;

  @ViewChild('imageInput')
  imageInput: ElementRef;
  editForm: FormGroup;
  logoArray: any[];
  languages: FormArray = new FormArray([]);
  private logoId: string;
  logoImages: any;
  logoFile: any;
  imageList: any[] = [];
  fileList: any[] = [];
  isLinkOld: boolean;
  isThumbnailOld: boolean; 
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private logoService: LogoService,
    private lightBox: Lightbox

  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.route.params
      .subscribe(
        (params: Params) => {
          this.logoId = params['logoid'];
          this.editForm = this.formBuilder.group({
            name: '',
            thumbnail: '',
            link: '',
            active: true,
            priority: 0,
          });
          this.logoService.getLogoById(+this.logoId)
            .subscribe(data => {
              let formData = [];
              this.logoFile = data.link;
              this.logoImages = data.thumbnail;
              formData['name'] = data.name;
              formData['active'] = data.active;
              formData['priority'] = data.priority;
              formData['thumbnail'] = data.thumbnail;
              formData['link'] = data.link;
              this.editForm.setValue(formData);
              this.editForm.value.isLinkOld = true;
              this.editForm.value.isThumbnailOld = true;

            });
        }
      )

    this.logoService.getLogo()
      .subscribe(data => {
        this.logoArray = data;
      });
  }

  showImage(image: any) {
    let album = [{
      src: image,
      caption: '',
      thumb: image
    }];
    this.lightBox.open(album, 0);
  }

  async handleImageInput(images: FileList) {
    for (let i = 0; i < images.length; i++) {
      this.imageList.push(images.item(i));
    }
    const data = await this.fileToBase64(this.imageList[0]);
    this.editForm.value.thumbnail = data;
    this.editForm.value.isLinkOld = false;
    this.imageInput.nativeElement.value = '';
  }

  async handleFileInput(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.fileList.push(files.item(i));
    }
    const fileData = await this.fileToBase64(this.fileList[0]);
    this.editForm.value.link = fileData;
    this.editForm.value.isThumbnailOld = false;
    this.fileInput.nativeElement.value = '';
  }

  async fileToBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });

  }

  async onSubmit() {
    this.logoService.updateLogo(this.logoId, this.editForm.value)
      .subscribe(
        data => {
          this.router.navigate(['logo']);
        },
        error => {
          alert(error);
        });
  }

}

