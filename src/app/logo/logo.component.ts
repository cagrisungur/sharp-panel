import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogoService } from '../_services/logo.service';


@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent  implements OnInit {

  logo: any[]; 
  
  constructor(
    private router: Router, 
    private logoService: LogoService
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.logoService.getLogo()
      .subscribe(data => {
        this.logo = data;
        setTimeout(() => {
          $('#dataTable').DataTable({
            responsive: true
          });
        }, 100);
      });
  }

  deleteLogo(logo: any): void {
    this.logoService.deleteLogo(logo.id)
      .subscribe(data => {
        this.logo = this.logo.filter(u => u !== logo);
        console.log(this.logo);
      })
  };

  editLogo(logo: any): void {
     this.router.navigate(['edit-logo', logo.id]);
  };

  addLogo(): void {
    this.router.navigate(['add-logo']);
  };
}
