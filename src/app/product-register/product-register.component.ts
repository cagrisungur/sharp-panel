import { Component, OnInit } from '@angular/core';
import { Features } from '../models/features.model';
import { Router } from '@angular/router';
import { ProductRegisterService } from '../_services/productresgister.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-product-register',
  templateUrl: './product-register.component.html',
  styleUrls: ['./product-register.component.scss']
})
export class ProductRegisterComponent implements OnInit {

  productRegister: any[];
  
  constructor(
    private router: Router,
    private productRegisterService: ProductRegisterService,
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.productRegisterService.getProductRegister()
      .subscribe(data => {
        this.productRegister = data;
        setTimeout(() => {
          $('#dataTable').DataTable({
            responsive: true
          });
        }, 100);
      });
  }
  
  editProductRegister(register: any): void {
    console.log(register);
    this.router.navigate(['register', register.id]);
  };
}
