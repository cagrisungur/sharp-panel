import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductRegisterService } from '../../_services/productresgister.service';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-register-product',
  templateUrl: './register-product.component.html',
  styleUrls: ['./register-product.component.scss']
})
export class RegisterProductComponent implements OnInit {

  productRegister: any[];
  registerId: any; 

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private productRegisterService: ProductRegisterService,
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    this.route.params
      .subscribe(params => {
        this.registerId = params["registerid"];

    this.productRegisterService.getProductRegisterById(this.registerId)
      .subscribe(data => {
        this.productRegister = data;
        setTimeout(() => {
          $('#dataTable').DataTable({
            responsive: true
          });
        }, 100);
      });
    });
  }

}
