import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { Router } from "@angular/router";
import { CategoryService } from '../../_services/category.service';
import { getAllService } from '../../_services/getAll.service';
import { LanguageService } from '../../_services/language.service';
import { NewsService } from '../../_services/news.service';
import { ViewChild, ElementRef } from '@angular/core';
import { ImageTypeService } from '../../_services/imagetype.service';
import { SpecificationService } from '../../_services/specification.service';
import { FileTypeService } from '../../_services/filetype.service';
import { FeaturesService } from '../../_services/features.service';
import { Lightbox } from 'ngx-lightbox';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {

  @ViewChild('fileInput')
  fileInput: ElementRef;

  @ViewChild('imageInput')
  imageInput: ElementRef;

  addForm: FormGroup;
  countries: any[];
  languages: any[];
  text: any[];
  news: any[];
  specs: any[];
  imageList: any[] = [];
  fileList: any[] = [];
  imageTypes: any[];
  fileTypes: any[];
  features: any[];
  featureTypes: any[];
  categoryFeature: any[];
  categorySpecs: any[];
  item: any[] = [];
  newsTitleImage: any;
  newsThumbnailsImage: any;
  newsHighResolutionImage: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private getAllService: getAllService,
    private imageTypeService: ImageTypeService,
    private newsService: NewsService,
    private lightBox: Lightbox

  ) { 
    this.newsTitleImage = "http://34.244.173.118:6060/uploads/images/news-title-photo.png";
    this.newsThumbnailsImage = "http://34.244.173.118:6060/uploads/images/news-thumbnail.png";
    this.newsHighResolutionImage = "http://34.244.173.118:6060/uploads/images/news-high-resuliton.png";

  }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.addForm = this.formBuilder.group({
      countryId: '',
      title: '',
      priority: '',
      active: true
    });

    this.getAllService.getCountries()
      .subscribe(data => {
        this.countries = data;
        this.text = this.initArray();
      }
      );

    this.newsService.getNews()
      .subscribe(data => {
        this.news = data;
      }
      );

    this.imageTypeService.getImageTypes()
      .subscribe(data => {
        this.imageTypes = data;
      });
  }

  public options: Object = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: false,
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert'],
  }
  
  handleImageInput(images: FileList) {
    for (let i = 0; i < images.length; i++) {
      this.imageList.push(images.item(i));
    }
    this.imageInput.nativeElement.value = '';
  }

  showImage(image: any) {
    let album = [{
      src: image,
      caption: '',
      thumb: image
    }];
    this.lightBox.open(album, 0);
  }

  async createImageListData() {
    let formArray = [];
    console.log(this.imageList);
    for (let index in this.imageList) {
      const data = await this.fileToBase64(this.imageList[index]);

      formArray.push({
        typeId: this.imageList[index].dType,
        name: this.imageList[index].dName,
        active: 1,
        priority: this.imageList[index].priority,
        imageFile: data
      });
    }

    return formArray;
  }

  async fileToBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  initArray() {
    var formArray = [];

    for (let i = 0; i < this.countries.length; i++) {
      formArray[this.countries[i].id] = [];
      for (let j = 0; j < this.countries[i].languages.length; j++) {
        formArray[this.countries[i].id][this.countries[i].languages[j].id] = {
          title: '',
          subtitle: '',
          description: '',
        };
      }
    }
    return formArray;
  }

  async onSubmit() {
    let postData = this.addForm.value;
    postData.text = this.createTextObject();
    postData.image = await this.createImageListData();
    postData.active = postData.active ? 1 : 0;

    this.newsService.createNews([postData])
      .subscribe(
        data => {
          this.router.navigate(['news']);
        },
        error => {
          alert(error);
        });
  }

  createTextObject() {
    let textArray = [];
    for (let i = 0; i < this.countries.length; i++) {
      for (let j = 0; j < this.countries[i].languages.length; j++) {
        var title = this.text[this.countries[i].id][this.countries[i].languages[j].id].title;
        var subtitle = this.text[this.countries[i].id][this.countries[i].languages[j].id].subtitle;
        var description = this.text[this.countries[i].id][this.countries[i].languages[j].id].description;
        if (this.text[this.countries[i].id][this.countries[i].languages[j].id].title &&
          this.text[this.countries[i].id][this.countries[i].languages[j].id].subtitle &&
          this.text[this.countries[i].id][this.countries[i].languages[j].id].description != ""
        )
          textArray.push({
            countryId: this.countries[i].id,
            langId: this.countries[i].languages[j].id,
            title: title,
            subtitle: subtitle,
            description: description,
          })
      }
    }
    return textArray;
  }

}


