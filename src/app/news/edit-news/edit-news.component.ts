import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { FormBuilder, FormGroup, FormArray } from "@angular/forms";
import { NewsService } from '../../_services/news.service';
import { getAllService } from '../../_services/getAll.service';
import { ViewChild, ElementRef } from '@angular/core';
import { ImageTypeService } from '../../_services/imagetype.service';
import { SpecificationService } from '../../_services/specification.service';
import { FileTypeService } from '../../_services/filetype.service';
import { Lightbox } from 'ngx-lightbox';
import { LanguageService } from '../../_services/language.service';
@Component({
  selector: 'app-edit-news-category',
  templateUrl: './edit-news.component.html',
  styleUrls: ['./edit-news.component.css']
})
export class EditNewsComponent implements OnInit {



  @ViewChild('imageInput')
  imageInput: ElementRef;

  editForm: FormGroup;
  countries: any[];
  languages: any[];
  textList: FormArray = new FormArray([]);
  text: any[];
  categories: any[];
  specs: any[];
  imageList: any[] = [];
  fileList: any[] = [];
  imageTypes: any[];
  fileTypes: any[];
  features: any[];
  featureTypes: any[];
  categoryFeature: any[];
  spec: any[];
  newsId: any;
  newsTitleImage: any;
  newsThumbnailsImage: any;
  newsHighResolutionImage: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private getAllService: getAllService,
    private imageTypeService: ImageTypeService,
    private newsService: NewsService,
    private route: ActivatedRoute,
    private lightBox: Lightbox,
    private languageService: LanguageService
  ) { 
    this.newsTitleImage = "http://34.244.173.118:6060/uploads/images/news-title-photo.png";
    this.newsThumbnailsImage = "http://34.244.173.118:6060/uploads/images/news-thumbnail.png";
    this.newsHighResolutionImage = "http://34.244.173.118:6060/uploads/images/news-high-resuliton.png";
  }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.route.params
      .subscribe(params => {
        this.newsId = params["newsid"];
        this.editForm = this.formBuilder.group({
          countryId: 0,
          title: '',
          priority: '',
          active: true
        });
        this.newsService.getNewsById(this.newsId)
          .subscribe(data => {
            let obj = data;
            let formData = {};
            formData['countryId'] = data.country.id;
            formData['title'] = data.title;
            formData['priority'] = data.priority;
            formData['active'] = data.active == 1 ? true : false;
            this.getAllService.getCountries()
              .subscribe(data => {
                this.countries = data;
                this.imageList = this.initImageList(obj);
                this.text = this.initArray(obj);

              });

            this.editForm.setValue(formData);
          });
      });

    this.editForm = this.formBuilder.group({
      countryId: 0,
      title: '',
      priority: '',
      active: true
    });


    this.languageService.getLanguages()
      .subscribe(data => {
        this.languages = data;
      }
      );


    this.imageTypeService.getImageTypes()
      .subscribe(data => {
        this.imageTypes = data;
      });
  }
  public options: Object = {
    placeholderText: 'Edit Your Content Here!',
    charCounterCount: false,
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat', 'alert'],
  }

  initArray(obj: any) {

    var formArray = [];

    for (let i = 0; i < this.countries.length; i++) {
      formArray[this.countries[i].id] = [];
      for (let j = 0; j < this.countries[i].languages.length; j++) {
        let filters = {
          countryId: this.countries[i].id,
          langId: this.countries[i].languages[j].id
        }
        let foundList = obj.text.filter(
          text => filters.countryId == obj.country.id && filters.langId == text.lang_id
        );
        formArray[this.countries[i].id][this.countries[i].languages[j].id] = foundList[0] ? foundList[0] : {
          title: '',
          subtitle: '',
          description: ''
        };
      }
    }
    return formArray;
  }




  initImageList(obj: any) {
    var formArray = [];
    for (let i = 0; i < obj.images.length; i++) {
      if (obj.images[i].path.endsWith('.jpeg') || obj.images[i].path.endsWith('.png') || obj.images[i].path.endsWith('.jpg')) {
        formArray.push({
          id: obj.images[i].id,
          dName: obj.images[i].name,
          imageFile: obj.images[i].path,
          dType: obj.images[i].type.id,
          active: obj.images[i].active,
          priority: obj.images[i].priority,
          convertBase64: false
        });
      }
    }
    return formArray;
  }

  async fileToBase64(file): Promise<any> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);

    });
  }


  handleImageInput(images: FileList) {
    for (let i = 0; i < images.length; i++) {
      this.imageList.push(images.item(i));
    }
    this.imageInput.nativeElement.value = '';
  }

  delete(images, imageIndex){
    if(this.imageList[imageIndex].convertBase64 == false){
      images.isDeleted = true; 

    }
    else{
      this.imageList.splice(imageIndex, 1);

    }
    
  }
  async createImageListData() {

    let formArray = [];
    let priority = 1;
    for (let index in this.imageList) {
      if (this.imageList[index].convertBase64 != false) {
        const data = await this.fileToBase64(this.imageList[index]);
        formArray.push({
          id: this.imageList[index].id,
          typeId: this.imageList[index].dType,
          name: this.imageList[index].dName,
          active: this.imageList[index].active,
          priority: this.imageList[index].priority,
          imageFile: data,
          isOld: false
        });
      } else {
        formArray.push({
          id: this.imageList[index].id,
          typeId: this.imageList[index].dType,
          name: this.imageList[index].dName,
          active: this.imageList[index].active,
          priority: this.imageList[index].priority,
          imageFile: this.imageList[index].path,
          isOld: true,
          isDeleted: this.imageList[index].isDeleted
        });
      }
    }

    return formArray;
  }

  showImage(image: any) {
    let album = [{
      src: image.imageFile,
      caption: '',
      thumb: image.imageFile
    }];
    this.lightBox.open(album, 0);
  }

  showImage2(image: any) {
    let album = [{
      src: image,
      caption: '',
      thumb: image
    }];
    this.lightBox.open(album, 0);
  }

  async onSubmit() {
    let postData = this.editForm.value;
    postData.text = this.createTextObject();
    postData.image = await this.createImageListData();
    postData.active = postData.active ? 1 : 0;
    this.newsService.updateNews(this.newsId, postData)
      .subscribe(
        data => {
          this.router.navigate(['news']);
        },
        error => {
          alert(error);
        });
  }

  createTextObject() {
    let textArray = [];
    for (let i = 0; i < this.countries.length; i++) {
      for (let j = 0; j < this.countries[i].languages.length; j++) {
        var title = this.text[this.countries[i].id][this.countries[i].languages[j].id].title;
        var subtitle = this.text[this.countries[i].id][this.countries[i].languages[j].id].subtitle;
        var description = this.text[this.countries[i].id][this.countries[i].languages[j].id].description;
        if (title || subtitle || description != ""){
          textArray.push({
            langId: this.countries[i].languages[j].id,
            title: title,
            subtitle: subtitle,
            description: description,
          })
        }
      }
    }
    return textArray;
  }



}


