import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { NewsService } from '../_services/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent  implements OnInit {

  news: any[]; 
  
  constructor(
    private router: Router, 
    private newsService: NewsService
  ) { }

  ngOnInit() {
    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';

    this.newsService.getNews()
      .subscribe(data => {
        this.news = data;
        setTimeout(() => {
          $('#dataTable').DataTable({
            responsive: true
          });
        }, 100);
      });
  }

  deleteNews(news: any): void {
    this.newsService.deleteNews(news.id)
      .subscribe(data => {
        this.news = this.news.filter(u => u !== news);
        console.log(this.news);
      })
  };

  editNews(news: any): void {
    console.log(news);
    this.router.navigate(['edit-news', news.id]);
  };

  addNews(): void {
    this.router.navigate(['add-news']);
  };
}
